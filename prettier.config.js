/** @type {import("prettier").Config} */
export default {
	printWidth: 120,
	semi: false,
	singleQuote: false,
	trailingComma: "all",
	useTabs: true,
	plugins: [
		"prettier-plugin-svelte",
		"prettier-plugin-tailwindcss", // MUST come last
	],
	overrides: [{ files: "*.svelte", options: { parser: "svelte" } }],
}
