import type { Reroute } from "@sveltejs/kit"
import { extractLangFromUrlPath } from "./lib/i18n/index"

export const reroute: Reroute = ({ url }) => {
	const urlPathLang = extractLangFromUrlPath(url.pathname)
	if (urlPathLang !== null) {
		return url.pathname.substring(3)
	}
}
