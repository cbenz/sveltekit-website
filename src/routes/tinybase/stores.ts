import { readable, writable, type Readable, type Writable } from "svelte/store"
import type { Content, Id, Store, Value } from "tinybase"
import { createSessionPersister } from "tinybase/persisters/persister-browser"
import { createSqlite3Persister } from "tinybase/persisters/persister-sqlite3"

export function createContentStore(store: Store): Readable<Content> {
	const initialValue = store.getContent()
	const _store = readable(initialValue, (set) => {
		const listenerId1 = store.addValueListener(null, () => {
			set(store.getContent())
		})
		const listenerId2 = store.addTablesListener(() => {
			set(store.getContent())
		})
		return () => {
			store.delListener(listenerId1)
			store.delListener(listenerId2)
		}
	})
	return _store
}

export function createSessionPersisterStore(...args: Parameters<typeof createSessionPersister>) {
	const persister = createSessionPersister(...args)
	return readable(persister, () => () => {
		persister.destroy()
	})
}

export function createSqlite3PersisterStore(...args: Parameters<typeof createSqlite3Persister>) {
	const persister = createSqlite3Persister(...args)
	return readable(persister, () => () => {
		persister.destroy()
	})
}

export function createValueStore<T extends Value>(store: Store, valueId: Id): Writable<T> {
	const initialValue = store.getValue(valueId)
	const { set, subscribe, update } = writable(initialValue, () => {
		const listenerId = store.addValueListener(valueId, (_store, _valueId, newValue) => {
			set(newValue)
		})
		return () => {
			store.delListener(listenerId)
		}
	})
	return {
		set(value) {
			store.setValue(valueId, value)
		},
		subscribe,
		update,
	}
}
