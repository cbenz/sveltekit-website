import type EditorJS from "@editorjs/editorjs"
import type { EditorConfig, OutputData } from "@editorjs/editorjs"
import type { Action } from "svelte/action"
import { readonly, writable } from "svelte/store"

export function useEditorJs() {
	const dataStore = writable<OutputData | null>(null)

	const editor: Action<HTMLDivElement, EditorConfig> = (element, config) => {
		let editor: EditorJS

		async function setData() {
			const outputData = await editor.save()
			dataStore.set(outputData)
		}

		async function init() {
			const { default: EditorJS } = await import("@editorjs/editorjs")
			editor = new EditorJS({
				...config,
				holder: element,
				onChange: async () => {
					await setData()
				},
			})
			await editor.isReady
			await setData()
		}

		init()

		return {
			destroy() {
				editor?.destroy()
			},
		}
	}

	return { data: readonly(dataStore), editor }
}
