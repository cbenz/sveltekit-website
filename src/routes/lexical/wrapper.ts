import { createEditor, type CreateEditorArgs, type LexicalEditor, type SerializedEditorState } from "lexical"
import type { Action } from "svelte/action"
import { readonly, writable } from "svelte/store"

export function useLexical() {
	const stateStore = writable<SerializedEditorState | null>(null)

	let resolve: (value: LexicalEditor) => void
	const ready = new Promise<LexicalEditor>((_resolve) => {
		resolve = _resolve
	})

	const lexical: Action<HTMLDivElement, CreateEditorArgs | undefined> = (element, params = {}) => {
		const editor = createEditor(params)
		resolve(editor)

		editor.registerUpdateListener(({ editorState }) => {
			stateStore.set(editorState.toJSON())
		})

		if (!element.isContentEditable) {
			element.contentEditable = "true"
		}
		editor.setRootElement(element)

		return {
			destroy() {
				editor.setRootElement(null)
			},
		}
	}

	return { lexical, ready, state: readonly(stateStore) }
}
