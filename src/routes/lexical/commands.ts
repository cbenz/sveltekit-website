import { $createQuoteNode } from "@lexical/rich-text"
import { $createTableCellNode, $createTableNode, $createTableRowNode, TableCellHeaderStates } from "@lexical/table"
import { $createTextNode, $getRoot } from "lexical"

export const appendQuote = (text: string) => () => {
	const root = $getRoot()
	const quote = $createQuoteNode()
	quote.append($createTextNode(text))
	root.append(quote)
}

export const appendTable = (text: string) => () => {
	const root = $getRoot()
	const table = $createTableNode()
	const header = $createTableRowNode()
	const headerCell = $createTableCellNode(TableCellHeaderStates.COLUMN)
	headerCell.append($createTextNode("Name"))
	header.append(headerCell)
	table.append(header)
	const row = $createTableRowNode()
	const rowCell = $createTableCellNode(TableCellHeaderStates.NO_STATUS)
	rowCell.append($createTextNode(text))
	row.append(rowCell)
	table.append(row)
	root.append(table)
}
