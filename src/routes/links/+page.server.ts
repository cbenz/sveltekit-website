import { fetchYaml } from "$lib/data-fetching/fetch-yaml"
import { logError } from "$lib/logging"
import { error } from "@sveltejs/kit"
import * as v from "valibot"

const LinksSchema = v.array(
	v.object({
		href: v.pipe(v.string(), v.nonEmpty()),
		image_url: v.optional(v.pipe(v.string(), v.url())),
		name: v.pipe(v.string(), v.nonEmpty()),
	}),
)
export type Links = v.InferOutput<typeof LinksSchema>

export const load = async ({ fetch }) => {
	let links
	try {
		links = await fetchYaml("/content/links.yml", { fetch, schema: LinksSchema })
	} catch (exc) {
		logError(exc)
		error(500, "Could not fetch links.")
	}
	return { links }
}
