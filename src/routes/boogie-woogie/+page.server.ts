import { fetchMarkdown } from "$lib/data-fetching/fetch-markdown"
import { logError } from "$lib/logging"
import { error } from "@sveltejs/kit"

export const load = async ({ fetch }) => {
	let html
	try {
		html = await fetchMarkdown("/content/boogie-woogie.md", { fetch })
	} catch (exc) {
		logError(exc)
		error(500, "Could not fetch Markdown file.")
	}
	return { html }
}
