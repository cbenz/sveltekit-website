import { env } from "$env/dynamic/public"
import { initLocales } from "$lib/i18n/index.js"
import { waitLocale } from "svelte-i18n"

export const load = async ({ data }) => {
	const { lang } = data
	initLocales({ fallbackLocale: env.PUBLIC_DEFAULT_LOCALE ?? "en", initialLocale: lang })
	await waitLocale()
	return data
}
