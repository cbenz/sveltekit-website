import { BlockDataSchema } from "$lib/components/blocks/types"
import * as v from "valibot"

const blocks = v.parse(
	v.array(BlockDataSchema),
	[
		{
			params: {
				level: 1,
				text: "Biography",
			},
			type: "heading",
		},
		{
			params: {
				text: "I'm Christophe Benz, woohoo!",
			},
			type: "paragraph",
		},
	].map((o) => ({ id: crypto.randomUUID(), ...o })),
)

export const load = async () => {
	return { blocks }
}
