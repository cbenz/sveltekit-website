import { pagesData } from "$lib/pages.js"
import { error } from "@sveltejs/kit"
import { _ } from "svelte-i18n"
import { get } from "svelte/store"

export const load = async ({ params: { page: pageSlug } }) => {
	const pageData = pagesData[pageSlug]
	if (pageData === undefined) {
		error(404, get(_)("http.status.404"))
	}
	return { pageData }
}
