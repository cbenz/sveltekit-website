import { fetchYaml } from "$lib/data-fetching/fetch-yaml"
import { logError } from "$lib/logging"
import { SettingsSchema } from "$lib/model/settings.js"
import { pagesData } from "$lib/pages"
import { error, type RequestEvent } from "@sveltejs/kit"
import { upperFirst } from "lodash-es"
import { _ } from "svelte-i18n"
import { get } from "svelte/store"

async function loadSettings({ fetch }: RequestEvent) {
	let settings

	try {
		settings = await fetchYaml("/content/settings.yml", { fetch, schema: SettingsSchema })
	} catch (exc) {
		logError(exc)
		error(500, "Could not fetch settings.")
	}

	for (const item of settings.nav.items) {
		if (item.text === undefined) {
			item.text = get(_)(pagesData[item.id]?.title) ?? upperFirst(item.id)
		}
	}

	return settings
}

export const load = async (event) => {
	const { lang, pb } = event.locals
	const settings = await loadSettings(event)
	const user = pb?.authStore.model
	return { lang, settings, user }
}
