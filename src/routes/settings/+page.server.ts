import { resolveRoute } from "$app/paths"
import { cookieName } from "$lib/hooks/lang.js"
import { redirect } from "@sveltejs/kit"
import * as v from "valibot"

export const actions = {
	setLang: async ({ cookies, request }) => {
		const FormDataSchema = v.object({
			lang: v.pipe(v.string(), v.nonEmpty()),
		})
		const formData = await request.formData()
		const inputData = Object.fromEntries(formData.entries())
		const { lang } = v.parse(FormDataSchema, inputData)
		cookies.set(cookieName, lang, { path: resolveRoute("/", {}) })
	},
}

export const load = () => {
	redirect(302, resolveRoute("/", {}))
}
