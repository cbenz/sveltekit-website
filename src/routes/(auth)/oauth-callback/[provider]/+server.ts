import { authProviderCookieName, buildRedirectPath } from "$lib/auth.js"
import { redirect } from "@sveltejs/kit"

export const GET = async ({ cookies, locals: { pb }, url }) => {
	if (pb === undefined) {
		throw new Error("PocketBase is disabled")
	}

	const authProvider = JSON.parse(cookies.get(authProviderCookieName) || "{}")

	if (authProvider.state !== url.searchParams.get("state")) {
		throw new Error("State parameters don't match")
	}

	try {
		await pb
			.collection("users")
			.authWithOAuth2Code(
				authProvider.name,
				url.searchParams.get("code") || "",
				authProvider.codeVerifier,
				url.origin + buildRedirectPath(authProvider.name),
			)
	} catch (error) {
		console.error(error)
		redirect(303, "/login?oauth_fail=true") // TODO keep redirect param
	}

	redirect(303, "/login") // TODO handle redirect
}
