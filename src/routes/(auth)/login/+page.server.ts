import { resolveRoute } from "$app/paths"
import { authProviderCookieName, buildRedirectPath, findAuthProvider } from "$lib/auth"
import { fail, redirect } from "@sveltejs/kit"
import * as v from "valibot"
import type { Action, Actions } from "./$types"

function makeOauthAction(authProviderName: string) {
	const action: Action = async ({ cookies, locals: { pb }, url }) => {
		if (pb === undefined) {
			throw new Error("PocketBase is disabled")
		}
		const authProvider = await findAuthProvider(authProviderName, { pb })
		const redirectPath = buildRedirectPath(authProvider.name)
		cookies.set(authProviderCookieName, JSON.stringify(authProvider), {
			httpOnly: true,
			path: redirectPath,
		})
		redirect(303, authProvider.authUrl + url.origin + redirectPath)
	}
	return action
}
export const actions = {
	login: async ({ locals: { pb }, request }) => {
		if (pb === undefined) {
			throw new Error("PocketBase is disabled")
		}

		const data = await request.formData()
		const LoginSchema = v.object({
			login: v.pipe(v.string(), v.nonEmpty()),
			password: v.pipe(v.string(), v.nonEmpty()),
		})
		const loginInputs = {
			login: data.get("login"),
		}
		const result = v.safeParse(LoginSchema, { ...loginInputs, password: data.get("password") })
		if (!result.success) {
			const loginValidationErrors = v.flatten(result.issues).nested
			return fail(400, { loginInputs, loginValidationErrors })
		}

		const { login, password } = result.output
		try {
			await pb.collection("users").authWithPassword(login, password)
		} catch {
			return fail(400, { loginAuthenticationError: true })
		}
	},
	oauth_github: makeOauthAction("github"),
} satisfies Actions

export const load = async ({ locals: { pb }, parent, url }) => {
	if (pb === undefined) {
		throw new Error("PocketBase is disabled")
	}

	const { user } = await parent()
	if (user !== null) {
		redirect(303, resolveRoute("/", {})) // TODO handle redirect
	}

	const authMethods = await pb.collection("users").listAuthMethods()
	const { authProviders } = authMethods

	const oauthFail = url.searchParams.get("oauth_fail") === "true"

	return { authProviders, oauthFail }
}
