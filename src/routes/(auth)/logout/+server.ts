import { resolveRoute } from "$app/paths"
import { redirect } from "@sveltejs/kit"

export const GET = ({ locals: { pb } }) => {
	if (pb === undefined) {
		throw new Error("PocketBase is disabled")
	}

	pb.authStore.clear()
	redirect(303, resolveRoute("/", {})) // TODO handle redirect
}
