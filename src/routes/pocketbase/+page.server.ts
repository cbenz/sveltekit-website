import { logError } from "$lib/logging"
import { fail } from "@sveltejs/kit"
import * as v from "valibot"
import type { Actions } from "./$types"

export const actions = {
	save: async ({ locals: { pb }, request }) => {
		if (pb === undefined) {
			throw new Error("PocketBase is disabled")
		}

		const FormDataSchema = v.object({
			linkOrder: v.pipe(
				v.string(),
				v.transform(JSON.parse),
				v.array(
					v.object({
						id: v.pipe(v.string(), v.nonEmpty()),
						order: v.pipe(v.number(), v.integer(), v.minValue(0)),
					}),
				),
			),
		})
		const formData = await request.formData()
		const inputData = Object.fromEntries(formData.entries())
		const { linkOrder } = v.parse(FormDataSchema, inputData)

		for (const { id, order } of linkOrder) {
			try {
				await pb.collection("nav_items").update(id, { order })
			} catch (exc) {
				logError(exc)
				return fail(400, { saveError: true })
			}
		}
	},
} satisfies Actions

export const load = async ({ fetch, locals: { pb } }) => {
	if (pb === undefined) {
		throw new Error("PocketBase is disabled")
	}

	const result = await pb.collection("nav_items").getList(undefined, undefined, { fetch, sort: "order" })
	return { result }
}
