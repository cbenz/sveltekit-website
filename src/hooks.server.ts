import { auth } from "$lib/hooks/auth"
import { lang } from "$lib/hooks/lang"
import { sequence } from "@sveltejs/kit/hooks"

export const handle = sequence(auth, lang)
