export function moveItemAfter<T>(array: T[], index: number): T[] {
	const item = array.splice(index, 1)[0]
	return [...array.slice(0, index + 1), item, ...array.slice(index + 1)]
}

export function moveItemBefore<T>(array: T[], index: number): T[] {
	const item = array.splice(index, 1)[0]
	return [...array.slice(0, index - 1), item, ...array.slice(index - 1)]
}
