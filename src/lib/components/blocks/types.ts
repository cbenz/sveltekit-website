import * as v from "valibot"
import { BlockParamsSchema as HeadingParamsSchema } from "./Heading.svelte"
import { BlockParamsSchema as ParagraphParamsSchema } from "./Paragraph.svelte"

const IdSchema = v.pipe(v.string(), v.uuid())

export const BlockDataSchema = v.variant("type", [
	v.object({
		id: IdSchema,
		params: HeadingParamsSchema,
		type: v.literal("heading"),
	}),
	v.object({
		id: IdSchema,
		params: ParagraphParamsSchema,
		type: v.literal("paragraph"),
	}),
])
export type BlockData = v.InferOutput<typeof BlockDataSchema>
