import NavLinks from "./NavLinks.svelte"

export interface NavLink {
	href: string
	id: string
	text: string
}

export interface Events {
	change: NavLink[]
}

export { NavLinks }
