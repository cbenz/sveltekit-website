import { PanelLeft, PanelTop } from "lucide-svelte"
import type { ComponentType } from "svelte"
import MobileLayout from "./MobileLayout.svelte"
import SideBarLayout from "./SideBarLayout.svelte"
import TopBarLayout from "./TopBarLayout.svelte"

export interface LayoutSpec {
	desktopComponent: ComponentType
	iconComponent: ComponentType
	mobileComponent: ComponentType
	text: string
}

export const layoutSpecMap = new Map<string, LayoutSpec>([
	[
		"topbar",
		{
			desktopComponent: TopBarLayout,
			iconComponent: PanelTop,
			mobileComponent: MobileLayout,
			text: "Topbar",
		},
	],
	[
		"sidebar",
		{
			desktopComponent: SideBarLayout,
			iconComponent: PanelLeft,
			mobileComponent: MobileLayout,
			text: "Sidebar",
		},
	],
])
