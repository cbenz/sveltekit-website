import { init, register } from "svelte-i18n"

export function extractLangFromUrlPath(path: string) {
	const match = path.match(/^\/([a-z]{2})\/(.*)/)
	return match ? match[1] : null
}

export function initLocales(...args: Parameters<typeof init>) {
	register("en", () => import("./locales/en.json"))
	register("fr", () => import("./locales/fr.json"))

	init({
		...args[0],
		handleMissingMessage(input) {
			console.error("Missing i18n message", input)
		},
	})
}
