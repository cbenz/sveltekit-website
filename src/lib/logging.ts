import { ValiError, flatten } from "valibot"

export function logError(error: unknown) {
	console.error(error)

	if (error instanceof ValiError) {
		console.error(flatten(error.issues))
	}
}
