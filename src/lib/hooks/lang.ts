import { extractLangFromUrlPath } from "$lib/i18n/index"
import { type Handle, type RequestEvent } from "@sveltejs/kit"

export const cookieName = "lang"
export const defaultLang = "en"

function detectLang({ cookies, request, url }: RequestEvent) {
	const urlPathLang = extractLangFromUrlPath(url.pathname)
	if (urlPathLang !== null) {
		console.log(`Using lang "${urlPathLang}" from URL path`)
		return urlPathLang
	}

	const cookieLang = cookies.get(cookieName)
	if (cookieLang) {
		console.log(`Using lang "${cookieLang}" from cookie`)
		return cookieLang
	}

	const requestHeaderLang = request.headers.get("accept-language")?.split(",")[0]
	if (requestHeaderLang) {
		console.log(`Using lang "${requestHeaderLang}" from accept-language request header`)
		return requestHeaderLang
	}

	console.log(`Using default lang "${defaultLang}"`)
	return defaultLang
}

export const lang: Handle = async ({ event, resolve }) => {
	const lang = detectLang(event)
	event.locals.lang = lang

	return await resolve(event, {
		transformPageChunk: ({ html }) => html.replace("%lang%", lang),
	})
}
