import { env } from "$env/dynamic/private"
import { logError } from "$lib/logging"
import { type Handle } from "@sveltejs/kit"
import PocketBase from "pocketbase"

export const auth: Handle = async ({ event, resolve }) => {
	const pocketBaseUrl = env.POCKETBASE_URL
	if (!pocketBaseUrl) {
		console.log("POCKETBASE_URL environment variable is not set, disabling PocketBase")
		return await resolve(event)
	}

	const pb = new PocketBase(pocketBaseUrl)
	event.locals.pb = pb

	pb.authStore.loadFromCookie(event.request.headers.get("cookie") || "")
	if (pb.authStore.isValid) {
		try {
			await pb.collection("users").authRefresh()
		} catch (exc) {
			logError(exc)
			pb.authStore.clear()
		}
	}

	const response = await resolve(event)
	response.headers.append("set-cookie", pb.authStore.exportToCookie())
	return response
}
