import * as v from "valibot"

export const defaultLayoutName = "sidebar"

export const SettingsSchema = v.object({
	layout: v.optional(
		v.object({
			name: v.optional(v.pipe(v.string(), v.nonEmpty()), defaultLayoutName),
		}),
		{},
	),
	nav: v.object({
		items: v.array(
			v.object({
				href: v.pipe(v.string(), v.nonEmpty()),
				id: v.pipe(v.string(), v.nonEmpty()),
				text: v.pipe(v.string(), v.nonEmpty()),
			}),
		),
	}),
	title: v.pipe(v.string(), v.nonEmpty()),
})
export type Settings = v.InferOutput<typeof SettingsSchema>
