import { resolveRoute } from "$app/paths"
import PocketBase from "pocketbase"

export const authProviderCookieName = "authProvider"

export function buildRedirectPath(authProviderName: string) {
	return resolveRoute(`/oauth-callback/[provider]`, { provider: authProviderName })
}

export async function findAuthProvider(authProviderName: string, { pb }: { pb: PocketBase }) {
	const { authProviders } = await pb.collection("users").listAuthMethods()
	const authProvider = authProviders.find((authProvider) => authProvider.name === authProviderName)
	if (authProvider === undefined) {
		throw new Error(`authProvider not found for "${authProviderName}"`)
	}
	return authProvider
}
