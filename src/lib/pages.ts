export type PageData = Record<string, string>
export type PagesData = Record<string, PageData>

export const pagesData: PagesData = {
	store: {
		title: "page.store.title",
		paragraph: "page.store.paragraph",
	},
}
