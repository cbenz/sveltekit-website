import * as v from "valibot"
import YAML from "yaml"

export async function fetchYaml<const TSchema extends v.BaseSchema<unknown, unknown, v.BaseIssue<unknown>>>(
	url: string,
	{ fetch, schema }: { fetch: typeof globalThis.fetch; schema: TSchema },
): Promise<v.InferOutput<TSchema>> {
	const response = await fetch(url)
	if (!response.ok) {
		throw response
	}
	const responseText = await response.text()
	const responseData = await YAML.parse(responseText)
	return await v.parseAsync(schema, responseData)
}
