import rehypeStringify from "rehype-stringify"
import remarkParse from "remark-parse"
import remarkRehype from "remark-rehype"
import { unified } from "unified"

export async function fetchMarkdown(url: string, { fetch }: { fetch: typeof globalThis.fetch }) {
	const response = await fetch(url)
	if (!response.ok) {
		throw response
	}
	const responseText = await response.text()
	const vfile = await unified().use(remarkParse).use(remarkRehype).use(rehypeStringify).process(responseText)
	return vfile.value
}
