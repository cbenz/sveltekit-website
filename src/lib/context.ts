import type { AuthModel } from "pocketbase"
import { getContext, setContext } from "svelte"
import type { Readable, Writable } from "svelte/store"
import type { Settings } from "./model/settings"

const contextKey = Symbol("LayoutContextKey")

export interface LayoutContext {
	editModeStore: Writable<boolean>
	mobileSidebarOpenStore: Writable<boolean>
	settingsStore: Readable<Settings>
	userStore: Readable<AuthModel>
}

export function getLayoutContext(): LayoutContext {
	return getContext(contextKey)
}

export function setLayoutContext(context: LayoutContext) {
	setContext(contextKey, context)
}
